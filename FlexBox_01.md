# Cours FlexBox 01

## What is FlexBox ?

* Une partie de CSS3. 
* Un ensemble de méthodes pour répondre aux besoins Responsive.
* LA solution aux problèmes d'alignement.

## Initialisation

```javascript
.container {
  display: flex; /* ou inline-flex */
}
```

## propriétés du container :

* Flex-direction :
    row (defaut): De gauche à droite
    row-reverse: de droite à gauche
    column: de haut en bas
    column-reverse: de bas en haut
    
* flex-flow :
    flex-start (defaut): entassé au point de départ
    flex-end: entassé sur la ligne fin
    center: centrés sur la ligne de milieu
    space-between: Distribué sur la ligne 
    space-around: 



## Exemple 1

```javascript
.container {
  /* histoire de bien voir */
  width: 500px;
  height: 500px;
  margin: 0 auto;
  background-color: #dedede;
  
  /* MODE FLEX */
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  align-items: center;
}



 .box
{
  width: 200px;
  height: 200px;
  margin: auto;
}
```

Le HTML :


```HTML

<div class="container">
  <div class="box">1</div>
  <div class="box">2</div>
  <div class="box">3</div>
</div>

```

```