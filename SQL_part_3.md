# mySQL Partie 3#

## SELECT (mode advanced) ##

Nous utilisons la BDD Sakila de Mysql : http://downloads.mysql.com/docs/sakila-db.zip

## AS (alias)

Permet de renommer les champs de sortie :

> SELECT (indentificateur) AS (alias) FROM (identificateur) WHERE (conditions)

Soit la requete de moyenne de longueur des films :

> SELECT COUNT(*),AVG(length)  FROM film 

on récupère la table suivante:

>|-----------|------------|

>|   COUNT(*)| AVG(length)|

>|-----------|------------|

>|       1230|       123,5|

>|-----------|------------|

On peut aliaser les sorties : 

> SELECT COUNT(*) as "nb_films" ,AVG(length) AS "moyenne"  FROM film 

|-----------|------------|
|   nb_films|     moyenne|
|-----------|------------|
|       1230|       123,5|
|-----------|------------|

Mais également les tables: 

> SELECT COUNT(t1.id_film) as "nb_films" ,AVG(t1.length) AS "moyenne"  FROM film as t1

### LEFT JOIN ###

permet une jointure de tout T1 avec T2 même si aucun enregistrement de T2 ne correspond.

>  SELECT 
>   film.film_id AS FID,
>   film.title AS title,
>   film.description AS description,
>   category.name AS category,
>   actor.first_name AS fname,
>   actor.last_name AS lname
>  FROM category 
>    LEFT JOIN film ON film_category.film_id = film.film_id
>    INNER JOIN film_actor ON film.film_id = film_actor.film_id
>    INNER JOIN actor ON film_actor.actor_id = actor.actor_id
>  ORDER BY category DESC

### GROUP BY & GROUP_CONCAT ###

Lors d'opération de regroupement, permet de choisir le champs sur lequel sera regroupé le retour.
Il s'agit d'un champ redondant (titre du film par ex.)

>  SELECT 
>   film.film_id AS FID,
>   film.title AS title,
>   film.description AS description,
>   GROUP_CONCAT(CONCAT(actor.first_name, _utf8' ', actor.last_name) SEPARATOR ', ') AS actors
>  FROM category 
>    LEFT JOIN film ON film_category.film_id = film.film_id
>    INNER JOIN film_actor ON film.film_id = film_actor.film_id
>    INNER JOIN actor ON film_actor.actor_id = actor.actor_id
> GROUP BY film.film_id;


